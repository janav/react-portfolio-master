import firebase from "firebase";

const firebaseApp = firebase.initializeApp({
  apiKey: "AIzaSyAtv5fRCxUUPUs89_Y4RgzJR7XzrmyB7cY",
  authDomain: "resume-portfolio-1ec05.firebaseapp.com",
  projectId: "resume-portfolio-1ec05",
  storageBucket: "resume-portfolio-1ec05.appspot.com",
  messagingSenderId: "231039179291",
  appId: "1:231039179291:web:c44f226712f293b597b6be",
  measurementId: "G-LH2PEQP7DM"
});

const db = firebaseApp.firestore();
const auth = firebase.auth();
const storage = firebase.storage();

export { db, auth, storage };
